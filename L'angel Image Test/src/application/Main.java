package application;
	
import javafx.application.Application;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;


public class Main extends Application {
	@Override
	public void start(Stage primaryStage) {
		try {
			Pane root = new Pane();
			Image img = new Image(getClass().getResourceAsStream("/imageSamples/Sample_1_2_A.png"));
			ImageView iTest = new ImageView();
			iTest.setImage(img);
			root.getChildren().add(iTest);
			Scene scene = new Scene(root,1920,1080);
			primaryStage.setScene(scene);
			primaryStage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	public static void main(String[] args) {
		launch(args);
	}
}
