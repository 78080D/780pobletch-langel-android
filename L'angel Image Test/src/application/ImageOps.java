package application;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class ImageOps {
	public ImageOps(Image image) {
		
	}
	private int analyzeWhitenessHC(int x, int y, int value) {
        double contrast = Math.pow((100 + value) / 100, 2);
        // get pixel color
        //int pixel = bImage.getPixel(x, y);

        // apply filter contrast for every channel R, G, B
        int red = red(pixel);
        red = (int)(((((red / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if(red < 0) red = 0;
        else if(red > 255) red = 255;

        int green = green(pixel);
        green = (int)(((((green / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if(green < 0) green = 0;
        else if(green > 255) green = 255;

        int blue = blue(pixel);
        blue = (int)(((((blue / 255.0) - 0.5) * contrast) + 0.5) * 255.0);
        if(blue < 0) blue = 0;
        else if(blue > 255) blue = 255;

        return (int)(0.299*red + 0.587*green + 0.0722*blue);
    }
	private int red(int pixel) {
        int rMask = 0xFF0000;
        return ((pixel & rMask) >> 16);
    }
    private int green(int pixel) {
        int gMask = 0xFF00;
        return ((pixel & gMask) >> 8);
    }
    private int blue(int pixel) {
        int bMask = 0xFF;
        return pixel & bMask;
    }
}
