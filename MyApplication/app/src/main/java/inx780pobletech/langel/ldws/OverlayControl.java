package inx780pobletech.langel.ldws;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.media.AudioFormat;
import android.media.AudioManager;
import android.media.AudioTrack;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import inx780pobletech.langel.CameraActivity;
import inx780pobletech.langel.CameraPreview;

@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class OverlayControl {
    private static final String TAG = "Overlay-Control";
    private ImageView view;
    private TextView[] debugLines;
    private Bitmap hud;
    private int width = 0, height = 0;
    private Canvas canvas;
    private Paint color = new Paint();
    //Alert system
    private AudioTrack audioTrack;
    private boolean audioTrackPlaying;
    public OverlayControl(ImageView imageView,  TextView[] debugLines) {
        view = imageView;
        this.debugLines = debugLines;
        setupAlert();
    }
    public void updateOverlay(int width, int height, double roll) {
        this.width = width;
        this.height = height;
        Log.d(TAG, "Registered width - " + width + " and height - " + height);
        initializeOverlay();
        setDebugLines();
        drawVehicleBounds();
        drawDetectedLanes();
        drawAlertOverlay();
        finishAndRenderOverlay();
    }
    private void initializeOverlay() {
        hud = Bitmap.createBitmap(this.width, this.height, Bitmap.Config.ARGB_8888);
        canvas = new Canvas(hud);
    }
    private void setDebugLines() {
        //debugLines[0].setText("Color - R: " + LDWSConfig.redValue + " G: " + LDWSConfig.greenValue + " B: " + LDWSConfig.blueValue + " W: " + LDWSConfig.whiteValue);
        debugLines[0].setText("AFPS: " + CameraPreview.AFPS + " \\\\ Average AFPS: " + CameraPreview.avgAFPS);
        debugLines[1].setText("ImageOps Threads: " + ImageOps.getInstances() + " " + "\\\\ Current White Threshold: " + LDWSConfig.LANE_ALPHA_THRESHOLD);
        debugLines[2].setText("Lane Left: " + LDWSConfig.whiteLeft + " || Lane Detected @: " + LDWSConfig.whiteLeftFound
                + " \\\\ Lane Right: " + LDWSConfig.whiteRight + " || Lane Detected @: " + LDWSConfig.whiteRightFound);
        debugLines[3].setText("Angle " + CameraActivity.roll);
    }
    private void drawVehicleBounds() {
        LDWSConfig.vehicle_left_x = (width/2) - (LDWSConfig.getFullVehicleWidthInches()/2);
        LDWSConfig.vehicle_right_x = (width/2) + (LDWSConfig.getFullVehicleWidthInches()/2);
        color.setColor(Color.argb(255, 244, 191, 66));
        canvas.drawLine(LDWSConfig.vehicle_left_x, 0, LDWSConfig.vehicle_left_x, height, color);
        canvas.drawLine(LDWSConfig.vehicle_right_x, 0, LDWSConfig.vehicle_right_x, height, color);
    }
    private void drawDetectedLanes() {
        int left_x = LDWSConfig.lane_left_x1;
        int right_x = LDWSConfig.lane_right_x1;
        //
        color.setColor(Color.CYAN);
        canvas.drawLine(left_x, 0, left_x, height, color);
        canvas.drawLine(right_x, 0, right_x, height, color);
        //
        int leftBoundX = (width/2) - (LDWSConfig.getFullVehicleWidthInches()/2);
        int rightBoundX = (width/2) + (LDWSConfig.getFullVehicleWidthInches()/2);
        int scanWidth = 30;
        color.setColor(Color.YELLOW);
        canvas.drawLine(leftBoundX - scanWidth, (height/3)*2, leftBoundX + scanWidth, (height/3)*2, color);
        canvas.drawLine(rightBoundX - scanWidth, (height/3)*2, rightBoundX + scanWidth, (height/3)*2, color);
    }
    private void drawAlertOverlay() {
        //Box///////////////////////////////////////////////////////////////////////////////////
        color.setColor(Color.GREEN);
        canvas.drawRect(0, 0, 4, height, color);                                //Left
        canvas.drawRect(0, 0, width, 4, color);                                 //Top
        canvas.drawRect(width-4, 0, width, height, color);                      //Right
        canvas.drawRect(0, height-4, width, height, color);                     //Bottom
        //Side//////////////////////////////////////////////////////////////////////////////////
        if(LDWSConfig.checkBoundsLeft() || LDWSConfig.checkBoundsRight()) {
            color.setColor(Color.YELLOW);
            if(LDWSConfig.checkBoundsLeft())
                canvas.drawRect(0, 0, 30, height, color);                        //Left
            if(LDWSConfig.checkBoundsRight())
                canvas.drawRect(width-30, 0, width, height, color);              //Right
            audioTrack.play();
        } else {
            audioTrack.stop();
        }
    }
    private void finishAndRenderOverlay() {
        view.setImageBitmap(hud);
    }
    /**Alert Sounds********************************************************************************/
    private void setupAlert() {
        int duration = 3, sampleRate = 8000;
        int numSamples = duration*sampleRate;
        double[] sample = new double[numSamples];
        double freq = 1020;                                                      //Change this to adjust the sound
        byte[] generatedSnd = new byte[2*numSamples];
        for(int i = 0; i < numSamples; ++i)
            sample[i] = Math.sin(2*Math.PI*i/ (sampleRate/freq));
        int idx = 0;
        for(final double dVal : sample) {
            final short val = (short) (dVal * 32767);
            generatedSnd[idx++] = (byte) (val & 0x00ff);
            generatedSnd[idx++] = (byte) ((val & 0xff00) >>> 8);
        }
        audioTrack = new AudioTrack(AudioManager.STREAM_MUSIC, sampleRate, AudioFormat.CHANNEL_OUT_MONO, AudioFormat.ENCODING_PCM_16BIT, generatedSnd.length, AudioTrack.MODE_STATIC);
        audioTrack.write(generatedSnd, 0, generatedSnd.length);
        audioTrack.setLoopPoints(0, 5, -1);
    }
}