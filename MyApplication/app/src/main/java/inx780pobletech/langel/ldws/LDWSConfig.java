package inx780pobletech.langel.ldws;

/**
 * Lane Departure Warning System Configuration
 *
 * Set by ConfigureActivity
 * Has default values anyways
 */

public class LDWSConfig {
    public static boolean DEBUG_MODE = true;                    //Display debug overlay
    //Constants work only for Honda CRV 2002, 2/3 of index finger from rear view mirror mount/base//
    public static int FEET_TO_PIXEL_CONST = 129;
    public static int INCHES_TO_PIXEL_CONST = 11;
    ////////////////////////////////////////////////////////////////////////////////////////////////
    public static int VEHICLE_WIDTH_FEET = 6;                   //2002 Honda CRV Width (Rounded)
    public static int VEHICLE_WIDTH_INCHES = 70;                //                     (Exact, off by .2 inches but that doesn't really matter)
    public static int LANE_ALPHA_THRESHOLD = 155;               //Alpha value/brightness to compare the lane color to, if the alpha detected is higher than this, then there is a lane
    public static int DESIRED_CAMERA_ANGLE = 80;                //Desired angle of the camera from the ground

    public static final int CONTRAST_CONST = 100;
    public static int redValue = 0, greenValue = 0, blueValue = 0, whiteValue = 0;
    //Vehicle Bounds////////////////////////////////////////////////////////////////////////////////
    public static int vehicle_left_x = 0, vehicle_right_x = 0;

    //Lane detection////////////////////////////////////////////////////////////////////////////////
    public static int whiteLeft = 0, whiteRight = 0;
    public static int whiteLeftFound = 0, whiteRightFound = 0;

    public static int lane_left_x1 = 0, lane_left_y1 = 0;
    public static int lane_left_x2 = 0, lane_left_y2 = 0;
    public static int lane_right_x1 = 0, lane_right_y1 = 0;
    public static int lane_right_x2 = 0, lane_right_y2 = 0;

    public static int getFullVehicleWidthInches() {
        return VEHICLE_WIDTH_INCHES*INCHES_TO_PIXEL_CONST;
    }
    public static int getFullVehicleWidthFeet() {
        return VEHICLE_WIDTH_FEET*FEET_TO_PIXEL_CONST;
    }
    public static boolean checkBoundsLeft() {
        return lane_left_x1 > vehicle_left_x;
    }
    public static boolean checkBoundsRight() {
        return lane_right_x1 < vehicle_right_x;
    }
}