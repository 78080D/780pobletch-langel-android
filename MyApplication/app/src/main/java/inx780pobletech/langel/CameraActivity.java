package inx780pobletech.langel;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.os.Bundle;
import android.util.Log;
import android.view.Window;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import inx780pobletech.langel.ldws.OverlayControl;

/**
 * Utilizes Camera API
 */
@SuppressWarnings("deprecation")
@RequiresApi(api = Build.VERSION_CODES.KITKAT)
public class CameraActivity extends Activity implements SensorEventListener {
    //Fixed-Constants///////////////////////////////////////////////////////////////////////////////
    private static final String TAG = "L'ANGEL-CAMERA";
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    //Camera Fields/////////////////////////////////////////////////////////////////////////////////
    private Camera mCamera;
    //Internal Fields///////////////////////////////////////////////////////////////////////////////
    private float[] mGravity;
    private float[] mGeomagnetic;
    private SensorManager mSensorManager;
    private Sensor mAccelerometer, mMagnetometer;
    public static double roll = 0;
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        setContentView(R.layout.activity_camera);
        //Initialize sensors
        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        mAccelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        mMagnetometer = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
        //Initialize & Register elements
        ImageView hudView = (ImageView) findViewById(R.id.cameraOverlay);
        //MediaPlayer mediaPlayer = MediaPlayer.create(this, null);
        TextView[] textViews = new TextView[4];
        textViews[0] = (TextView) findViewById(R.id.cameraTextDebugLine1);
        textViews[1] = (TextView) findViewById(R.id.cameraTextDebugLine2);
        textViews[2] = (TextView) findViewById(R.id.cameraTextDebugLine3);
        textViews[3] = (TextView) findViewById(R.id.cameraTextDebugLine4);
        OverlayControl overlay = new OverlayControl(hudView, textViews);
        //Add permission for camera and let user grant the permission
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE}, REQUEST_CAMERA_PERMISSION);
            return;
        }
        //Initialize camera
        if(checkCameraHardware(this)) {
            mCamera = getCameraInstance();
            //Enable autofocus on the camera - Doesn't work on samsung devices...
            Camera.Parameters mCameraParameters = mCamera.getParameters();
            Log.d(TAG, mCameraParameters.getSupportedFocusModes().toString());
            mCameraParameters.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);
            mCamera.setParameters(mCameraParameters);

            Log.d(TAG, "Camera Object: " + mCamera);
            //mCamera.setPreviewCallback(mPreviewCallback);

            CameraPreview mPreview = new CameraPreview(this, mCamera, overlay);
            FrameLayout preview = (FrameLayout) findViewById(R.id.camera_preview);
            preview.addView(mPreview);
        } else {
            Toast.makeText(CameraActivity.this, "This app requires a camera installed in order to function", Toast.LENGTH_LONG).show();
            finish();
        }
    }
    @Override protected void onResume() {
        super.onResume();
        mSensorManager.registerListener(this, mAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        mSensorManager.registerListener(this, mMagnetometer, SensorManager.SENSOR_DELAY_NORMAL);
    }
    @Override protected void onPause() {
        super.onPause();
        mSensorManager.unregisterListener(this);
    }
    @Override protected void onDestroy() {
        super.onDestroy();
        releaseCamera();
    }
    /**Check if this device has a camera*/
    private boolean checkCameraHardware(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA);
    }
    /**A safe way to get an instance of the Camera object*/
    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open(Camera.CameraInfo.CAMERA_FACING_BACK); //attempt to get a Camera instance
        } catch (Exception e){
            e.printStackTrace();
        }
        return c; //returns null if camera is unavailable
    }
    /**Releases the camera once the menu is exited*/
    private void releaseCamera() {
        if(mCamera != null) {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }
    /**Sensor class*/
    @Override public void onSensorChanged(SensorEvent event) {
        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER)
            mGravity = event.values;
        if (event.sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD)
            mGeomagnetic = event.values;
        if (mGravity != null && mGeomagnetic != null) {
            float R[] = new float[9];
            float I[] = new float[9];
            boolean success = SensorManager.getRotationMatrix(R, I, mGravity, mGeomagnetic);
            if (success) {
                float orientation[] = new float[3];
                SensorManager.getOrientation(R, orientation);
                //azimuth = orientation[0]; // orientation contains: azimuth, pitch and roll
                //pitch = orientation[1];
                roll = Math.round(Math.toDegrees(orientation[2]));
            }
        }
    }
    @Override public void onAccuracyChanged(Sensor sensor, int i) {
    }
}
