package inx780pobletech.langel;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.SeekBar;
import android.widget.TextView;

import inx780pobletech.langel.ldws.LDWSConfig;

public class ConfigureActivity extends AppCompatActivity {
    private SeekBar vehicleWidth;
    private SeekBar alphaThreshold;
    private TextView displayWidth;
    private TextView displayAlpha;
    private int currentWidth = LDWSConfig.VEHICLE_WIDTH_FEET;
    private int currentAlpha = LDWSConfig.LANE_ALPHA_THRESHOLD;
    private final float ALPHA_CONVERSION_CONST = (float) 2.56;
    @Override protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_configure);

        vehicleWidth = (SeekBar) findViewById(R.id.vehicle_width);
        alphaThreshold = (SeekBar) findViewById(R.id.alpha_threshold);
        displayWidth = (TextView) findViewById(R.id.vehicle_width_value);
        displayAlpha = (TextView) findViewById(R.id.alpha_value);
        vehicleWidth.setOnSeekBarChangeListener(widthListener);
        alphaThreshold.setOnSeekBarChangeListener(alphaListener);
        vehicleWidth.setProgress(LDWSConfig.VEHICLE_WIDTH_FEET *10);
        alphaThreshold.setProgress((int)(LDWSConfig.LANE_ALPHA_THRESHOLD/ALPHA_CONVERSION_CONST));
    }
    public void launchCameraFromCam(View view) {
        finish();
        setConfiguration();
        Intent intent = new Intent(this, CameraActivity.class);
        startActivity(intent);
    }
    public void exitConfig(View view) {
        finish();
    }
    private SeekBar.OnSeekBarChangeListener widthListener = new SeekBar.OnSeekBarChangeListener() {
        @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            currentWidth = Math.round(progress/10);
            displayWidth.setText(currentWidth + "");
        }
        @Override public void onStartTrackingTouch(SeekBar seekBar) {
        }
        @Override public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };
    private SeekBar.OnSeekBarChangeListener alphaListener = new SeekBar.OnSeekBarChangeListener() {
        @Override public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
            currentAlpha = (int) (progress*ALPHA_CONVERSION_CONST);
            displayAlpha.setText(currentAlpha + "");
        }
        @Override public void onStartTrackingTouch(SeekBar seekBar) {
        }
        @Override public void onStopTrackingTouch(SeekBar seekBar) {
        }
    };
    private void setConfiguration() {
        LDWSConfig.VEHICLE_WIDTH_FEET = currentWidth;
        LDWSConfig.LANE_ALPHA_THRESHOLD = currentAlpha;
    }
}
